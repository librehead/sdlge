#include "color.hpp"

sdlge::color_t::color_t(int const t_r, int const t_g, int const t_b,
                        int const t_a) noexcept
    : m_r{t_r}
    , m_g{t_g}
    , m_b{t_b}
    , m_a{t_a}
{
}

bool sdlge::color_t::operator==(sdlge::color_t const& t_color) const noexcept
{
    return m_r == t_color.red() && m_g == t_color.green() &&
           m_b == t_color.blue() && m_a == t_color.alpha();
}

bool sdlge::color_t::operator!=(sdlge::color_t const& t_color) const noexcept
{
    return !this->operator==(t_color);
}
