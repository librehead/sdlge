#include <cstdint>

#include "sdlge.hpp"

#include "SDL2/SDL.h"
#include "SDL2/SDL_ttf.h"

sdlge::application_t::application_t(int const t_width, int const t_height,
                                    char const* const t_title) noexcept
    : m_window{t_width, t_height, t_title}
    , m_renderer{m_window}
    , m_event{}
    , m_fps{0.0f}
    , m_avg_fps{0.0f}
{
}

sdlge::application_t::application_t(application_t& t_other) noexcept
    : m_window{t_other.window()}
    , m_renderer{t_other.renderer()}
    , m_event{}
    , m_fps{0.0f}
    , m_avg_fps{0.0f}
{
}

void sdlge::application_t::render_text(text_t& t_text,
                                       vec2 const& t_pos) noexcept
{
    SDL_Color color;
    color.r = static_cast<Uint8>(t_text.color().red());
    color.g = static_cast<Uint8>(t_text.color().green());
    color.b = static_cast<Uint8>(t_text.color().blue());
    color.a = static_cast<Uint8>(t_text.color().alpha());

    SDL_Rect dest_rect;
    dest_rect.x = t_pos.x;
    dest_rect.y = t_pos.y;
    dest_rect.w = t_text.width();
    dest_rect.h = t_text.height();

    SDL_RenderCopy(m_renderer.get(), t_text.get_texture(), nullptr, &dest_rect);
}

void sdlge::application_t::loop()
{
    bool volatile running{true};

    std::int64_t timer_start = SDL_GetTicks();

    while(running && !m_event.quit()) {
        std::int64_t const timer_end = SDL_GetTicks();
        float elapsed_time =
            static_cast<float>(timer_end - timer_start) / 1000.f;
        m_fps = (elapsed_time <= 0.000001f) ? 99999.f : 1.f / elapsed_time;
        m_avg_fps += m_fps;
        m_avg_fps /= 2;
        timer_start = timer_end;

        m_event.reset();
        m_event.update();

        m_renderer.clear();

        running = this->update(elapsed_time);

        m_renderer.update();
    }
}
