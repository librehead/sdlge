#include "vec2.hpp"

sdlge::vec2::vec2(int const t_x, int const t_y) noexcept
    : x{t_x}
    , y{t_y}
{
}
