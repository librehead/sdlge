#include "SDL2/SDL.h"
#include "SDL2/SDL_ttf.h"

#include "font.hpp"
#include "renderer.hpp"

sdlge::font_t::font_t(char const* const t_path, int const t_size,
                      renderer_t& t_renderer) noexcept
    : m_font{TTF_OpenFont(t_path, t_size)}
    , m_renderer{t_renderer}
{
    if(!m_font) {
        SDL_Log("Couldn't load font: %s\n", SDL_GetError());
    }
}

sdlge::font_t::~font_t() noexcept
{
    TTF_CloseFont(m_font);
    m_font = nullptr;
}
