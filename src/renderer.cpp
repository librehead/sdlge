#include "renderer.hpp"
#include "SDL2/SDL.h"
#include "window.hpp"

sdlge::renderer_t::renderer_t(sdlge::window_t& t_window) noexcept
    : m_renderer{nullptr}
    , m_window{t_window}
    , m_clear_color{0, 0, 0, 255}
    , m_borrowed_resource{false}
{
    m_renderer =
        SDL_CreateRenderer(m_window.get(), -1, SDL_RENDERER_ACCELERATED);

    if(!m_renderer) {
        SDL_Log("Couldn't create renderer: %s\n", SDL_GetError());
    }
}

sdlge::renderer_t::renderer_t(renderer_t& t_other) noexcept
    : m_renderer{t_other.get()}
    , m_window{t_other.window()}
    , m_clear_color{0, 0, 0, 255}
    , m_borrowed_resource{true}
{
}

sdlge::renderer_t::~renderer_t() noexcept
{
    if(!m_borrowed_resource) {
        SDL_DestroyRenderer(m_renderer);
    }
}

void sdlge::renderer_t::set_render_color(sdlge::color_t const& t_color) noexcept
{
    SDL_SetRenderDrawColor(m_renderer, t_color.red(), t_color.green(),
                           t_color.blue(), t_color.alpha());
}

void sdlge::renderer_t::render_point(int const t_x, int const t_y,
                                     sdlge::color_t const& t_color) noexcept
{
    this->set_render_color(t_color);
    SDL_RenderDrawPoint(m_renderer, t_x, t_y);
}

void sdlge::renderer_t::render_line(int const t_x0, int const t_y0,
                                    int const t_x1, int const t_y1,
                                    sdlge::color_t const& t_color) noexcept
{
    this->set_render_color(t_color);
    SDL_RenderDrawLine(m_renderer, t_x0, t_y0, t_x1, t_y1);
}

void sdlge::renderer_t::render_rectangle(int const t_x, int const t_y,
                                         int const t_w, int const t_h,
                                         sdlge::color_t const& t_color) noexcept
{
    this->set_render_color(t_color);

    SDL_Rect rect;
    rect.x = t_x;
    rect.y = t_y;
    rect.w = t_w;
    rect.h = t_h;

    SDL_RenderFillRect(m_renderer, &rect);
}

void sdlge::renderer_t::render_outline_rectangle(
    int const t_x, int const t_y, int const t_w, int const t_h,
    sdlge::color_t const& t_color) noexcept
{
    this->set_render_color(t_color);

    SDL_Rect rect;
    rect.x = t_x;
    rect.y = t_y;
    rect.w = t_w;
    rect.h = t_h;

    SDL_RenderDrawRect(m_renderer, &rect);
}

void sdlge::renderer_t::render_triangle(int const t_x0, int const t_y0,
                                        int const t_x1, int const t_y1,
                                        int const t_x2, int const t_y2,
                                        sdlge::color_t const& t_color) noexcept
{
    this->set_render_color(t_color);

    auto SWAP = [](int& x, int& y) {
        int t = x;
        x = y;
        y = t;
    };
    auto drawline = [&](int sx, int ex, int ny) {
        SDL_RenderDrawLine(m_renderer, sx, ny, ex, ny);
    };

    auto x1 = t_x0;
    auto y1 = t_y0;

    auto x2 = t_x1;
    auto y2 = t_y1;

    auto x3 = t_x2;
    auto y3 = t_y2;

    int t1x, t2x, y, minx, maxx, t1xp, t2xp;
    bool changed1 = false;
    bool changed2 = false;
    int signx1, signx2, dx1, dy1, dx2, dy2;
    int e1, e2;
    // Sort vertices
    if(y1 > y2) {
        SWAP(y1, y2);
        SWAP(x1, x2);
    }
    if(y1 > y3) {
        SWAP(y1, y3);
        SWAP(x1, x3);
    }
    if(y2 > y3) {
        SWAP(y2, y3);
        SWAP(x2, x3);
    }

    t1x = t2x = x1;
    y = y1; // Starting points
    dx1 = (int)(x2 - x1);
    if(dx1 < 0) {
        dx1 = -dx1;
        signx1 = -1;
    }
    else
        signx1 = 1;
    dy1 = (int)(y2 - y1);

    dx2 = (int)(x3 - x1);
    if(dx2 < 0) {
        dx2 = -dx2;
        signx2 = -1;
    }
    else
        signx2 = 1;
    dy2 = (int)(y3 - y1);

    if(dy1 > dx1) { // swap values
        SWAP(dx1, dy1);
        changed1 = true;
    }
    if(dy2 > dx2) { // swap values
        SWAP(dy2, dx2);
        changed2 = true;
    }

    e2 = (int)(dx2 >> 1);
    // Flat top, just process the second half
    if(y1 == y2)
        goto next;
    e1 = (int)(dx1 >> 1);

    for(int i = 0; i < dx1;) {
        t1xp = 0;
        t2xp = 0;
        if(t1x < t2x) {
            minx = t1x;
            maxx = t2x;
        }
        else {
            minx = t2x;
            maxx = t1x;
        }
        // process first line until y value is about to change
        while(i < dx1) {
            i++;
            e1 += dy1;
            while(e1 >= dx1) {
                e1 -= dx1;
                if(changed1)
                    t1xp = signx1; // t1x += signx1;
                else
                    goto next1;
            }
            if(changed1)
                break;
            else
                t1x += signx1;
        }
        // Move line
    next1:
        // process second line until y value is about to change
        while(1) {
            e2 += dy2;
            while(e2 >= dx2) {
                e2 -= dx2;
                if(changed2)
                    t2xp = signx2; // t2x += signx2;
                else
                    goto next2;
            }
            if(changed2)
                break;
            else
                t2x += signx2;
        }
    next2:
        if(minx > t1x)
            minx = t1x;
        if(minx > t2x)
            minx = t2x;
        if(maxx < t1x)
            maxx = t1x;
        if(maxx < t2x)
            maxx = t2x;
        drawline(minx, maxx, y); // Draw line from min to max points found on
                                 // the y Now increase y
        if(!changed1)
            t1x += signx1;
        t1x += t1xp;
        if(!changed2)
            t2x += signx2;
        t2x += t2xp;
        y += 1;
        if(y == y2)
            break;
    }
next:
    // Second half
    dx1 = (int)(x3 - x2);
    if(dx1 < 0) {
        dx1 = -dx1;
        signx1 = -1;
    }
    else
        signx1 = 1;
    dy1 = (int)(y3 - y2);
    t1x = x2;

    if(dy1 > dx1) { // swap values
        SWAP(dy1, dx1);
        changed1 = true;
    }
    else
        changed1 = false;

    e1 = (int)(dx1 >> 1);

    for(int i = 0; i <= dx1; i++) {
        t1xp = 0;
        t2xp = 0;
        if(t1x < t2x) {
            minx = t1x;
            maxx = t2x;
        }
        else {
            minx = t2x;
            maxx = t1x;
        }
        // process first line until y value is about to change
        while(i < dx1) {
            e1 += dy1;
            while(e1 >= dx1) {
                e1 -= dx1;
                if(changed1) {
                    t1xp = signx1;
                    break;
                } // t1x += signx1;
                else
                    goto next3;
            }
            if(changed1)
                break;
            else
                t1x += signx1;
            if(i < dx1)
                i++;
        }
    next3:
        // process second line until y value is about to change
        while(t2x != x3) {
            e2 += dy2;
            while(e2 >= dx2) {
                e2 -= dx2;
                if(changed2)
                    t2xp = signx2;
                else
                    goto next4;
            }
            if(changed2)
                break;
            else
                t2x += signx2;
        }
    next4:

        if(minx > t1x)
            minx = t1x;
        if(minx > t2x)
            minx = t2x;
        if(maxx < t1x)
            maxx = t1x;
        if(maxx < t2x)
            maxx = t2x;
        drawline(minx, maxx, y);
        if(!changed1)
            t1x += signx1;
        t1x += t1xp;
        if(!changed2)
            t2x += signx2;
        t2x += t2xp;
        y += 1;
        if(y > y3)
            return;
    }
}

void sdlge::renderer_t::render_outline_triangle(
    int const t_x0, int const t_y0, int const t_x1, int const t_y1,
    int const t_x2, int const t_y2, sdlge::color_t const& t_color) noexcept
{
    this->set_render_color(t_color);

    SDL_RenderDrawLine(m_renderer, t_x0, t_y0, t_x1, t_y1);
    SDL_RenderDrawLine(m_renderer, t_x1, t_y1, t_x2, t_y2);
    SDL_RenderDrawLine(m_renderer, t_x2, t_y2, t_x0, t_y0);
}

void sdlge::renderer_t::render_circle(int const t_x, int const t_y,
                                      int const t_radius,
                                      sdlge::color_t const& t_color) noexcept
{
    auto xc = t_x;
    auto yc = t_y;
    auto r = t_radius;

    int x = 0;
    int y = r;
    int p = 3 - 2 * r;
    if(!r)
        return;

    this->set_render_color(t_color);

    auto drawline = [&](int sx, int ex, int ny) {
        SDL_RenderDrawLine(m_renderer, sx, ny, ex, ny);
    };

    while(y >= x) {
        // Modified to draw scan-lines instead of edges
        drawline(xc - x, xc + x, yc - y);
        drawline(xc - y, xc + y, yc - x);
        drawline(xc - x, xc + x, yc + y);
        drawline(xc - y, xc + y, yc + x);
        if(p < 0)
            p += 4 * x++ + 6;
        else
            p += 4 * (x++ - y--) + 10;
    }
}

void sdlge::renderer_t::render_outline_circle(
    int const t_x, int const t_y, int const t_radius,
    sdlge::color_t const& t_color) noexcept
{
    this->set_render_color(t_color);

    auto Draw = [&](const int t_x, const int t_y) {
        SDL_RenderDrawPoint(m_renderer, t_x, t_y);
    };

    auto r = t_radius;
    auto xc = t_x;
    auto yc = t_y;

    int x = 0;
    int y = r;
    int p = 3 - 2 * r;
    if(!r)
        return;

    while(y >= x) // only formulate 1/8 of circle
    {
        Draw(xc - x, yc - y); // upper left left
        Draw(xc - y, yc - x); // upper upper left
        Draw(xc + y, yc - x); // upper upper right
        Draw(xc + x, yc - y); // upper right right
        Draw(xc - x, yc + y); // lower left left
        Draw(xc - y, yc + x); // lower lower left
        Draw(xc + y, yc + x); // lower lower right
        Draw(xc + x, yc + y); // lower right right
        if(p < 0)
            p += 4 * x++ + 6;
        else
            p += 4 * (x++ - y--) + 10;
    }
}

void sdlge::renderer_t::set_clear_color(color_t const& t_color) noexcept
{
    m_clear_color = t_color;
}

void sdlge::renderer_t::clear() noexcept
{
    this->set_render_color(m_clear_color);
    SDL_RenderClear(m_renderer);
}

void sdlge::renderer_t::update() noexcept
{
    SDL_RenderPresent(m_renderer);
}

SDL_Renderer* sdlge::renderer_t::get() noexcept
{
    return m_renderer;
}

SDL_Renderer* sdlge::renderer_t::get() const noexcept
{
    return m_renderer;
}
