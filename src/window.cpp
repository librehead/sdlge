#include "window.hpp"
#include "SDL2/SDL.h"
#include "SDL2/SDL_ttf.h"

void sdlge::window_t::initialize_sdl() noexcept
{
    if(SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Could not initialize sdl window: %s\n", SDL_GetError());
    }

    m_window = SDL_CreateWindow(m_title, SDL_WINDOWPOS_UNDEFINED,
                                SDL_WINDOWPOS_UNDEFINED, m_width, m_height,
                                SDL_WINDOW_SHOWN);

    if(!m_window) {
        SDL_Log("Could not create window: %s\n", SDL_GetError());
    }

    SDL_SetWindowResizable(m_window, SDL_FALSE);

    if(TTF_Init() == -1) {
        SDL_Log("Could not initialize ttf: %s\n", TTF_GetError());
    }
}

sdlge::window_t::window_t() noexcept
    : m_width{640}
    , m_height{480}
    , m_window{nullptr}
    , m_title{"SDLGE window"}
    , m_borrowed_resource{false}
{
    this->initialize_sdl();
}

sdlge::window_t::window_t(int const t_width, int const t_height,
                          char const* const t_title) noexcept
    : m_width{t_width}
    , m_height{t_height}
    , m_window{nullptr}
    , m_title{t_title}
    , m_borrowed_resource{false}
{
    this->initialize_sdl();
}

sdlge::window_t::window_t(window_t const& t_other)
    : m_width{t_other.width()}
    , m_height{t_other.height()}
    , m_window{t_other.get()}
    , m_title{t_other.title()}
    , m_borrowed_resource{true}
{
}

sdlge::window_t::~window_t() noexcept
{
    if(!m_borrowed_resource) {
        SDL_DestroyWindow(m_window);
        TTF_Quit();

        // if the window is closed, the entire app is considererd as closed,
        // so quit sdl completely here
        SDL_Quit();
    }
}

SDL_Window* sdlge::window_t::get() noexcept
{
    return m_window;
}

SDL_Window* sdlge::window_t::get() const noexcept
{
    return m_window;
}

SDL_Surface* sdlge::window_t::get_surface() noexcept
{
    return SDL_GetWindowSurface(m_window);
}

SDL_Surface* sdlge::window_t::get_surface() const noexcept
{
    return SDL_GetWindowSurface(m_window);
}

void sdlge::window_t::update_window_title() noexcept
{
    SDL_SetWindowTitle(m_window, m_title);
}

void sdlge::window_t::set_title(char const* const t_title) noexcept
{
    m_title = t_title;
    this->update_window_title();
}
