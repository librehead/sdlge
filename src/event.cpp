#include "event.hpp"

void sdlge::event_t::update()
{
    SDL_Event event;
    while(SDL_PollEvent(&event)) {
        switch(event.type) {
        case SDL_QUIT:
            m_quit = true;
            break;
        case SDL_KEYDOWN:
            if(event.key.repeat == 0) {
                m_keys_pressed[event.key.keysym.scancode] = true;
                m_keys_held[event.key.keysym.scancode] = true;
            }
            break;
        case SDL_KEYUP:
            m_keys_released[event.key.keysym.scancode] = true;
            m_keys_held[event.key.keysym.scancode] = false;
            break;
        default:
            break;
        }
    }

    m_mouse_state = SDL_GetMouseState(&m_mouse_x, &m_mouse_y);
}

void sdlge::event_t::reset() noexcept
{
    m_keys_pressed.clear();
    m_keys_released.clear();
}

namespace {
    template<typename M>
    bool exists(M const& t_map, SDL_Scancode const t_key) noexcept
    {
        if(t_map.find(t_key) == t_map.end()) {
            return false;
        }

        return t_map.at(t_key);
    }
} // namespace

bool sdlge::event_t::was_key_pressed(SDL_Scancode const t_key) const noexcept
{
    return exists(m_keys_pressed, t_key);
}

bool sdlge::event_t::is_key_held(SDL_Scancode const t_key) const noexcept
{
    return exists(m_keys_held, t_key);
}

bool sdlge::event_t::was_key_released(SDL_Scancode const t_key) const noexcept
{
    return exists(m_keys_held, t_key);
}
