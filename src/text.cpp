#include "text.hpp"
#include "font.hpp"
#include "renderer.hpp"

sdlge::text_t::text_t(font_t& t_font, char const* const t_str,
                      color_t const& t_color) noexcept
    : m_font{t_font}
    , m_color{}
    , m_texture{nullptr}
    , m_dimension{}
{
    SDL_Color color;
    color.r = t_color.red();
    color.g = t_color.green();
    color.b = t_color.blue();
    color.a = t_color.alpha();

    SDL_Surface* text_surface =
        TTF_RenderText_Solid(m_font.get(), t_str, color);

    m_texture =
        SDL_CreateTextureFromSurface(m_font.get_renderer().get(), text_surface);

    SDL_QueryTexture(m_texture, nullptr, nullptr, &m_dimension.x,
                     &m_dimension.y);
}

sdlge::text_t::~text_t() noexcept
{
    SDL_DestroyTexture(m_texture);
}
