cmake_minimum_required( VERSION 3.5 )

project( sdlge )

find_package( SDL2 REQUIRED )

link_directories( ${SDL2_LIBRARY_DIRS} )

set( SRC_FILES 
    ${CMAKE_CURRENT_SOURCE_DIR}/include/window.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/include/color.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/include/renderer.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/include/sdlge.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/include/event.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/include/vec2.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/include/font.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/include/text.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/color.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/event.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/renderer.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/sdlge.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/window.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/vec2.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/font.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/text.cpp
)

set( SDLGE_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/include ${SDL2_INCLUDE_DIRS} )

include_directories( ${SDLGE_INCLUDE_DIRS} )

add_library( sdlge_lib ${SRC_FILES} )

target_include_directories( sdlge_lib PUBLIC ${SDL2_INCLUDE_DIRS} )
target_link_libraries( sdlge_lib ${SDL2_LIBRARIES} SDL2_ttf )

function( add_example EXAMPLE_NAME EXAMPLE_FILE )
    add_executable( ${EXAMPLE_NAME} ${EXAMPLE_FILE} )
    target_link_libraries( ${EXAMPLE_NAME} sdlge_lib )
endfunction()

add_example( basic ${CMAKE_CURRENT_SOURCE_DIR}/examples/basic.cpp ) 
add_example( fps ${CMAKE_CURRENT_SOURCE_DIR}/examples/fps.cpp ) 
add_example( escape ${CMAKE_CURRENT_SOURCE_DIR}/examples/escape.cpp ) 
add_example( point ${CMAKE_CURRENT_SOURCE_DIR}/examples/point.cpp ) 
add_example( line ${CMAKE_CURRENT_SOURCE_DIR}/examples/line.cpp ) 
add_example( rect ${CMAKE_CURRENT_SOURCE_DIR}/examples/rect.cpp ) 
add_example( triangle ${CMAKE_CURRENT_SOURCE_DIR}/examples/triangle.cpp ) 
add_example( circle ${CMAKE_CURRENT_SOURCE_DIR}/examples/circle.cpp ) 
add_example( move_rect ${CMAKE_CURRENT_SOURCE_DIR}/examples/move_rect.cpp ) 
add_example( rect_chasing_mouse ${CMAKE_CURRENT_SOURCE_DIR}/examples/rect_chasing_mouse.cpp ) 
add_example( snake ${CMAKE_CURRENT_SOURCE_DIR}/examples/snake.cpp ) 
add_example( snake_menu ${CMAKE_CURRENT_SOURCE_DIR}/examples/snake_menu.cpp ) 
add_example( font ${CMAKE_CURRENT_SOURCE_DIR}/examples/font.cpp ) 
add_example( sierpinski ${CMAKE_CURRENT_SOURCE_DIR}/examples/sierpinski.cpp ) 
add_example( koch ${CMAKE_CURRENT_SOURCE_DIR}/examples/koch.cpp ) 
add_example( random_color ${CMAKE_CURRENT_SOURCE_DIR}/examples/random_color.cpp )
add_example( sorting ${CMAKE_CURRENT_SOURCE_DIR}/examples/sorting.cpp )

