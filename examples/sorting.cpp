#include "sdlge.hpp"

#include <algorithm>
#include <iostream>
#include <random>
#include <vector>

namespace {
    constexpr int width{1440};
    constexpr int height{900};
} // namespace

float avg_fps;

class app_t : public sdlge::application_t
{
  private:
    std::vector<int> m_numbers{};

    static constexpr int m_count{550};

    void initialize()
    {
        m_numbers.resize(m_count);

        int i = 1;

        for(int& n : m_numbers) {
            n = i++;
        }

        std::mt19937 rng{std::random_device{}()};
        std::shuffle(m_numbers.begin(), m_numbers.end(), rng);
    }

    void draw()
    {
        float constexpr ratio_w = float{width} / m_count;
        float constexpr ratio_h = float{height} / m_count;

        int i = 1;

        for(int const n : m_numbers) {
            float const start_x = ratio_w * (i - 1);
            float const start_y = height - ratio_h * n;
            float const rect_w = ratio_w - 1;
            float const rect_h = ratio_h * n;

            rectangle(start_x, start_y, rect_w, rect_h, sdlge::RED);

            ++i;
        }
    }

    struct bubble_sort_t
    {
      private:
        int m_index = 0;
        bool m_sorted = false;
        bool m_curr_iter_sorted = true;

      public:
        bubble_sort_t() noexcept = default;
        ~bubble_sort_t() noexcept = default;

        bool sorted() const noexcept
        {
            return m_sorted;
        }

        void update(std::vector<int>& t_numbers) noexcept
        {
            if(this->sorted()) {
                return;
            }
            if(m_index >= m_count) {
                if(m_curr_iter_sorted) {
                    m_sorted = true;
                    return;
                }
                else {
                    m_index = 0;
                    m_curr_iter_sorted = true;
                    return;
                }
            }

            if(t_numbers[m_index - 1] > t_numbers[m_index]) {
                std::swap(t_numbers[m_index - 1], t_numbers[m_index]);
                m_curr_iter_sorted = false;
            }

            ++m_index;
        }
    } bubble_sort;

  protected:
    virtual bool update(float const) noexcept override
    {
        bubble_sort.update(m_numbers);
        this->draw();

        avg_fps = query_avg_fps();

        return true;
    }

  public:
    app_t()
        : sdlge::application_t{width, height, "Sorting"}
    {
        this->initialize();
    }
    virtual ~app_t() noexcept = default;
};

int main()
{
    app_t sorting;

    sorting.loop();

    std::cout << "FPS: " << avg_fps << std::endl;

    return 0;
}
