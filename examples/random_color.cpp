#include <iostream>
#include <random>

#include "sdlge.hpp"

namespace {
    int constexpr g_width{400};
    int constexpr g_height{400};

    float avg_fps{0.f};
} // namespace

class rand_t : public sdlge::application_t
{
  private:
    std::mt19937_64 m_rng;
    std::uniform_int_distribution<unsigned char> m_dist{0, 255};

  protected:
    virtual bool update(float const) noexcept override
    {
        for(int i = 0; i < g_width; ++i) {
            for(int j = 0; j < g_height; ++j) {
                point(i, j,
                      sdlge::color_t{m_dist(m_rng), m_dist(m_rng),
                                     m_dist(m_rng)});
            }
        }
        avg_fps = query_avg_fps();
        return true;
    }

  public:
    rand_t()
        : application_t{}
    {
        m_rng.seed(std::random_device{}());
    }
};

int main()
{
    rand_t app;

    app.loop();

    std::cout << "FPS: " << avg_fps << '\n';
}
