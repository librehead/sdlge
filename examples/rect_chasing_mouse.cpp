#include "sdlge.hpp"

#include <cmath>

namespace {
    int constexpr g_width{1280};
    int constexpr g_height{720};
} // namespace

class example_t : public sdlge::application_t
{
  private:
    // speed in pixels per second
    static int constexpr m_x_vel{300};
    static int constexpr m_y_vel{300};

    float m_rect_x{0.0f};
    float m_rect_y{0.0f};
    int const m_rect_w{400};
    int const m_rect_h{400};

  public:
    example_t()
        : application_t{g_width, g_height, "Hey there"}
    {
        m_rect_x = g_width / 2.f - m_rect_w / 2.f;
        m_rect_y = g_height / 2.f - m_rect_h / 2.f;
    }
    virtual ~example_t() noexcept override = default;

  protected:
    virtual bool update(float const t_elapsed_time) noexcept override
    {
        if(key_pressed(SDL_SCANCODE_ESCAPE)) {
            return false;
        }

        set_title((std::string{"FPS: "} + std::to_string(query_fps())).c_str());

        float const center_x = m_rect_x + m_rect_w / 2.f;
        float const center_y = m_rect_y + m_rect_h / 2.f;

        float const dest_x = mouse_x();
        float const dest_y = mouse_y();

        float const dx = dest_x - center_x;
        float const dy = dest_y - center_y;

        float const distance = std::sqrt(dx * dx + dy * dy);

        if(distance > 5) {
            m_rect_x += dx * m_x_vel * t_elapsed_time / distance;
            m_rect_y += dy * m_y_vel * t_elapsed_time / distance;
        }

        rectangle(m_rect_x, m_rect_y, m_rect_w, m_rect_h);

        return true;
    }
};

int main()
{
    example_t ex;

    ex.loop();
}
