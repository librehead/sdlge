#include <cmath>
#include <vector>

#include "sdlge.hpp"

namespace {
    int constexpr g_width{900};
    int constexpr g_height{900};
} // namespace

struct vec2f
{
    float x{0.f};
    float y{0.f};

    constexpr vec2f() noexcept = default;
    vec2f(float const t_x, float const t_y) noexcept
        : x{t_x}
        , y{t_y}
    {
    }
    vec2f(vec2f const&) noexcept = default;
    vec2f(vec2f&&) noexcept = default;
    ~vec2f() noexcept = default;

    vec2f& operator=(vec2f const&) noexcept = default;
    vec2f& operator=(vec2f&&) noexcept = default;
};

struct triangle_t
{
    vec2f a, b, c;

    triangle_t() noexcept = default;
    triangle_t(vec2f const& t_a, vec2f const& t_b, vec2f const& t_c) noexcept
        : a{t_a}
        , b{t_b}
        , c{t_c}
    {
    }
    ~triangle_t() noexcept = default;
};

class sierpinski_t : public sdlge::application_t
{
  private:
    std::vector<triangle_t> m_triangles;
    std::size_t m_counter{0};

    enum
    {
        LIMIT = 9
    };

    void draw_all() noexcept
    {
        for(triangle_t t : m_triangles) {
            outline_triangle(static_cast<int>(t.a.x), static_cast<int>(t.a.y),
                             static_cast<int>(t.b.x), static_cast<int>(t.b.y),
                             static_cast<int>(t.c.x), static_cast<int>(t.c.y),
                             sdlge::GREY);
        }
    }

    void draw_triangle(std::size_t const n, vec2f const& t_a, vec2f const& t_b,
                       vec2f const& t_c)
    {
        if(n > LIMIT) {
            return;
        }

        m_triangles.emplace_back(t_a, t_b, t_c);

        vec2f d{(t_a.x + t_b.x) / 2.f, (t_a.y + t_b.y) / 2.f};
        vec2f e{(t_a.x + t_c.x) / 2.f, (t_a.y + t_b.y) / 2.f};
        vec2f f{(t_b.x + t_c.x) / 2.f, (t_b.y + t_c.y) / 2.f};

        draw_triangle(n + 1, t_a, d, e);
        draw_triangle(n + 1, d, t_b, f);
        draw_triangle(n + 1, e, f, t_c);
    }

    void construct_sierpinski()
    {
        draw_triangle(1, vec2f{g_width / 2.f, 0.f}, vec2f{0.f, g_height},
                      vec2f{g_width, g_height});
    }

  protected:
    virtual bool update(float const) noexcept override
    {
        this->draw_all();

        return true;
    }

  public:
    sierpinski_t()
        : application_t{g_width, g_height, "Sierpinski fractal!"}
        , m_triangles{}
        , m_counter{0}
    {
        m_triangles.reserve(std::pow(3.f, LIMIT) + 10);
        this->construct_sierpinski();
    }
};

int main()
{
    sierpinski_t app;

    app.loop();
}
