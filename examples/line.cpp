#include "sdlge.hpp"

namespace {
    int constexpr g_width{1280};
    int constexpr g_height{720};
} // namespace

class example_t : public sdlge::application_t
{
  public:
    example_t()
        : application_t{g_width, g_height, "Hey there"}
    {
    }
    virtual ~example_t() noexcept override = default;

  protected:
    virtual bool update(float const) noexcept override
    {
        if(key_pressed(SDL_SCANCODE_ESCAPE)) {
            return false;
        }

        set_title((std::string{"FPS: "} + std::to_string(query_fps())).c_str());

        line(50, 60, 300, 400);

        line(1200, 60, 1200, 700, sdlge::BROWN);

        return true;
    }
};

int main()
{
    example_t ex;

    ex.loop();
}
