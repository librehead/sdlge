#include "sdlge.hpp"

class example_t : public sdlge::application_t
{
  protected:
    virtual bool update(float const) noexcept override
    {
        if(key_pressed(SDL_SCANCODE_ESCAPE)) {
            return false;
        }

        return true;
    }

  public:
    example_t()
        : application_t{900, 600, "Example"}
    {
    }
    virtual ~example_t() noexcept override = default;
};

int main()
{
    example_t ex;

    ex.loop();
}
