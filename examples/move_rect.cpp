#include "sdlge.hpp"

namespace {
    int constexpr g_width{1280};
    int constexpr g_height{720};
} // namespace

class example_t : public sdlge::application_t
{
  private:
    // speed in pixels per second
    static int constexpr m_x_vel{300};
    static int constexpr m_y_vel{300};

    float m_rect_x{0.0f};
    float m_rect_y{0.0f};
    int const m_rect_w{400};
    int const m_rect_h{400};

  public:
    example_t()
        : application_t{g_width, g_height, "Hey there"}
    {
        m_rect_x = g_width / 2.f - m_rect_w / 2.f;
        m_rect_y = g_height / 2.f - m_rect_h / 2.f;
    }
    virtual ~example_t() noexcept override = default;

  protected:
    virtual bool update(float const t_elapsed_time) noexcept override
    {
        if(key_pressed(SDL_SCANCODE_ESCAPE)) {
            return false;
        }

        set_title((std::string{"FPS: "} + std::to_string(query_fps())).c_str());

        if(key_held(SDL_SCANCODE_UP)) {
            m_rect_y -= m_y_vel * t_elapsed_time;
        }
        else if(key_held(SDL_SCANCODE_DOWN)) {
            m_rect_y += m_y_vel * t_elapsed_time;
        }
        else if(key_held(SDL_SCANCODE_LEFT)) {
            m_rect_x -= m_x_vel * t_elapsed_time;
        }
        else if(key_held(SDL_SCANCODE_RIGHT)) {
            m_rect_x += m_x_vel * t_elapsed_time;
        }

        rectangle(m_rect_x, m_rect_y, m_rect_w, m_rect_h, sdlge::CYAN);

        return true;
    }
};

int main()
{
    example_t ex;

    ex.loop();
}
