#include "sdlge.hpp"

class app : public sdlge::application_t
{
  public:
    app()
        : application_t{1280, 720, "Hey there"}
    {
    }
    virtual ~app() noexcept override = default;

  protected:
    virtual bool update(float const) noexcept override
    {
        return true;
    }
};

int main()
{
    app m;

    m.loop();

    return 0;
}
