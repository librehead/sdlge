#include <cmath>
#include <vector>

#include "sdlge.hpp"

namespace {
    int constexpr g_width{900};
    int constexpr g_height{900};

    int constexpr DEPTH{6};
} // namespace

struct vec2f
{
    float x{0.f};
    float y{0.f};

    constexpr vec2f() noexcept = default;
    vec2f(float const t_x, float const t_y) noexcept
        : x{t_x}
        , y{t_y}
    {
    }
    vec2f(vec2f const&) noexcept = default;
    vec2f(vec2f&&) noexcept = default;
    ~vec2f() noexcept = default;

    vec2f& operator=(vec2f const&) noexcept = default;
    vec2f& operator=(vec2f&&) noexcept = default;
};

struct line_t
{
    vec2f a, b;

    line_t() noexcept = default;
    line_t(vec2f const& t_a, vec2f const& t_b) noexcept
        : a{t_a}
        , b{t_b}
    {
    }
    ~line_t() noexcept = default;
};

class koch_t : public sdlge::application_t
{
  private:
    std::vector<line_t> m_lines;

    void draw_all() noexcept
    {
        for(line_t t : m_lines) {
            line(static_cast<int>(t.a.x), static_cast<int>(t.a.y),
                 static_cast<int>(t.b.x), static_cast<int>(t.b.y), sdlge::GREY);
        }
    }

    void draw_lines(std::size_t const n, vec2f const& t_a, vec2f const& t_b)
    {
        if(n > DEPTH) {
            m_lines.emplace_back(t_a, t_b);
            return;
        }

        float const dx = t_b.x - t_a.x;
        float const dy = t_b.y - t_a.y;

        vec2f const c{t_a.x + dx / 3.f, t_a.y + dy / 3.f};
        vec2f const d{
            //                    | change signs for inward/outward
            (t_a.x + t_b.x) / 2.f - static_cast<float>(std::sqrt(3)) * dy / 6.f,
            (t_a.y + t_b.y) / 2.f +
                static_cast<float>(std::sqrt(3)) * dx / 6.f};
        vec2f const e{t_a.x + 2 * dx / 3.f, t_a.y + 2 * dy / 3.f};

        draw_lines(n + 1, t_a, c);
        draw_lines(n + 1, c, d);
        draw_lines(n + 1, d, e);
        draw_lines(n + 1, e, t_b);
    }

    void construct_koch()
    {
        // vec2f const a{ g_width / 3.f, g_height / 3.f };
        // vec2f const b{ 2 * g_width / 3.f, g_height / 3.f };
        // vec2f const c{ g_width / 2.f, 2 * g_height / 3.f };
        //
        // draw_lines(
        //    1, a, b
        //);
        // draw_lines(
        //    1, b, c
        //);
        // draw_lines(
        //    1, c, a
        //);
        vec2f const a{g_width / 4.f, g_height / 4.f};
        vec2f const b{3 * g_width / 4.f, g_height / 4.f};
        vec2f const c{3 * g_width / 4.f, 3 * g_height / 4.f};
        vec2f const d{g_width / 4.f, 3 * g_height / 4.f};

        draw_lines(1, a, b);
        draw_lines(1, b, c);
        draw_lines(1, c, d);
        draw_lines(1, d, a);
    }

  protected:
    virtual bool update(float const) noexcept override
    {
        this->draw_all();

        return true;
    }

  public:
    koch_t()
        : application_t{g_width, g_height, "Koch fractal!"}
        , m_lines{}
    {
        m_lines.reserve(4 * std::pow(4.f, DEPTH));
        this->construct_koch();
    }
};

int main()
{
    koch_t app;

    app.loop();
}
