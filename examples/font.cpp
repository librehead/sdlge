#include "sdlge.hpp"

class example_t : public sdlge::application_t
{
  private:
    sdlge::font_t m_font;
    sdlge::text_t m_text;

  protected:
    virtual bool update(float const) noexcept
    {
        sdlge::vec2 pos;

        pos.x = 1280 / 2 - m_text.width() / 2;
        pos.y = 720 / 2 - m_text.height() / 2;

        render_text(m_text, pos);

        return true;
    }

  public:
    example_t()
        : application_t{1280, 720, "Font"}
        , m_font{load_font(
              "../res/fonts/elaine-sans/hinted-ElaineSans-Light.ttf", 16)}
        , m_text{m_font, "Hello there!", sdlge::RED}
    {
    }
};

int main()
{
    example_t app{};

    app.loop();
}
