#include "sdlge.hpp"

#include <iostream>
#include <string>

class example_t : public sdlge::application_t
{
  private:
    float m_fps;

  protected:
    virtual bool update(float const) noexcept override
    {
        m_fps = this->query_avg_fps();
        auto s = std::to_string(this->query_fps());
        auto title = std::string{"FPS: "} + s;
        this->set_title(title.c_str());
        return true;
    }

  public:
    example_t()
        : application_t{1280, 720, "FPS:"}
    {
    }
    virtual ~example_t() noexcept override
    {
        try {
            std::cout << m_fps << std::endl;
        }
        catch(...) {
        }
    }
};

int main()
{
    example_t ex;

    ex.loop();
}
