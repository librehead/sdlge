#include "sdlge.hpp"

namespace {
    int constexpr g_width{1280};
    int constexpr g_height{720};
} // namespace

class example_t : public sdlge::application_t
{
  public:
    example_t()
        : application_t{g_width, g_height, "Hey there"}
    {
    }
    virtual ~example_t() noexcept override = default;

  protected:
    virtual bool update(float const) noexcept override
    {
        if(key_pressed(SDL_SCANCODE_ESCAPE)) {
            return false;
        }

        set_title((std::string{"FPS: "} + std::to_string(query_fps())).c_str());

        outline_rectangle(20, 20, g_width - 40, g_height - 40, sdlge::BLUE);
        rectangle(120, 120, g_width - 240, g_height - 240, sdlge::RED);

        return true;
    }
};

int main()
{
    example_t ex;

    ex.loop();
}
