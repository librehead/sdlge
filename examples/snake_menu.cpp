#include <array>
#include <iostream>
#include <list>
#include <memory>
#include <random>
#include <string>

#include "sdlge.hpp"

std::vector<std::unique_ptr<sdlge::application_t>> g_scenes;

namespace {
    int constexpr g_width{900};
    int constexpr g_height{900};

    int constexpr g_field_width{10};
    int constexpr g_field_height{10};

    int constexpr g_tile_width{g_width / g_field_width};
    int constexpr g_tile_height{g_height / g_field_height};

    int constexpr g_max_wait_time_ms{150};
} // namespace

struct vec2
{
    int i{0};
    int j{0};

    constexpr vec2() noexcept = default;
    vec2(int const t_i, int const t_j)
        : i{t_i}
        , j{t_j}
    {
    }
    vec2(vec2 const&) noexcept = default;
    vec2(vec2&&) noexcept = default;
    ~vec2() noexcept = default;

    vec2& operator=(vec2 const&) noexcept = default;
    vec2& operator=(vec2&&) noexcept = default;
};

class field_t
{
  private:
    template<typename T, int Rows, int Cols>
    using matrix = std::array<std::array<T, Cols>, Rows>;

  public:
    enum cell_type : int
    {
        HEAD = 0,
        BODY,
        FRUIT,
        EMPTY
    };

  private:
    matrix<cell_type, g_field_height, g_field_width> m_field;

    using data_t = matrix<cell_type, g_field_height, g_field_width>;

  public:
    field_t() noexcept
    {
        this->clear();
    };
    ~field_t() noexcept = default;

    cell_type& operator()(int const t_i, int const t_j) noexcept
    {
        return m_field[t_i][t_j];
    }

    cell_type operator()(int const t_i, int const t_j) const noexcept
    {
        return m_field[t_i][t_j];
    }

    data_t& data() noexcept
    {
        return m_field;
    }

    data_t const& data() const noexcept
    {
        return m_field;
    }

    void clear() noexcept
    {
        for(auto& line : m_field) {
            line.fill(cell_type::EMPTY);
        }
    }
};

class fruit_t
{
  private:
    field_t& m_field;

    vec2 m_position;

    vec2 generate_new_position() const noexcept
    {
        std::mt19937_64 rng;
        rng.seed(std::random_device{}());
        std::uniform_int_distribution<int> dist_row{0, g_field_height - 1};
        std::uniform_int_distribution<int> dist_col{0, g_field_width - 1};

        vec2 pos{dist_row(rng), dist_col(rng)};

        while(m_field(pos.i, pos.j) != field_t::cell_type::EMPTY) {
            pos.i = dist_row(rng);
            pos.j = dist_col(rng);
        }

        return pos;
    }

  public:
    fruit_t() = delete;
    fruit_t(field_t& t_field) noexcept
        : m_field{t_field}
        , m_position{-1, -1}
    {
    }
    ~fruit_t() noexcept = default;

    void new_position() noexcept
    {
        m_position = this->generate_new_position();
    }

    void update() noexcept
    {
        m_field(m_position.i, m_position.j) = field_t::cell_type::FRUIT;
    }

    vec2 const& get_position() const noexcept
    {
        return m_position;
    }
};

class snake_t
{
  private:
    field_t& m_field;
    fruit_t& m_fruit;

    std::list<vec2> m_positions{};

    bool m_died;

    void update_snake_on_field() noexcept
    {
        m_field(m_positions.front().i, m_positions.front().j) =
            field_t::cell_type::HEAD;

        auto it = m_positions.begin();
        ++it;

        for(auto i = it; i != m_positions.end(); ++i) {
            m_field((*i).i, (*i).j) = field_t::cell_type::BODY;
        }
    }

    void lengthen_snake_up() noexcept
    {
        m_positions.emplace_front(m_positions.front().i - 1,
                                  m_positions.front().j);
    }
    void lengthen_snake_down() noexcept
    {
        m_positions.emplace_front(m_positions.front().i + 1,
                                  m_positions.front().j);
    }
    void lengthen_snake_left() noexcept
    {
        m_positions.emplace_front(m_positions.front().i,
                                  m_positions.front().j - 1);
    }
    void lengthen_snake_right() noexcept
    {
        m_positions.emplace_front(m_positions.front().i,
                                  m_positions.front().j + 1);
    }

    void pop_snake_body() noexcept
    {
        m_positions.pop_back();
    }

  public:
    snake_t() = delete;
    snake_t(field_t& t_field, fruit_t& t_fruit) noexcept
        : m_field{t_field}
        , m_fruit{t_fruit}
        , m_positions{}
        , m_died{false}
    {
        m_positions.emplace_back(g_field_height / 2, g_field_width / 2);
        m_positions.emplace_back(g_field_height / 2 + 1, g_field_width / 2);
        m_positions.emplace_back(g_field_height / 2 + 2, g_field_width / 2);
    }

    void move_up() noexcept
    {
        vec2 head_pos = m_positions.front();
        vec2 new_pos{head_pos.i - 1, head_pos.j};

        if(m_field(new_pos.i, new_pos.j) == field_t::cell_type::BODY) {
            m_died = true;
            return;
        }
        if(new_pos.i < 0) {
            m_died = true;
            return;
        }

        if(m_field(new_pos.i, new_pos.j) == field_t::cell_type::FRUIT) {
            m_fruit.new_position();
            this->lengthen_snake_up();
            return;
        }

        this->lengthen_snake_up();
        this->pop_snake_body();
    }
    void move_down() noexcept
    {
        vec2 head_pos = m_positions.front();
        vec2 new_pos{head_pos.i + 1, head_pos.j};

        if(m_field(new_pos.i, new_pos.j) == field_t::cell_type::BODY) {
            m_died = true;
            return;
        }
        if(new_pos.i >= g_field_height) {
            m_died = true;
            return;
        }

        if(m_field(new_pos.i, new_pos.j) == field_t::cell_type::FRUIT) {
            m_fruit.new_position();
            this->lengthen_snake_down();
            return;
        }

        this->lengthen_snake_down();
        this->pop_snake_body();
    }
    void move_left() noexcept
    {
        vec2 head_pos = m_positions.front();
        vec2 new_pos{head_pos.i, head_pos.j - 1};

        if(m_field(new_pos.i, new_pos.j) == field_t::cell_type::BODY) {
            m_died = true;
            return;
        }
        if(new_pos.j < 0) {
            m_died = true;
            return;
        }

        if(m_field(new_pos.i, new_pos.j) == field_t::cell_type::FRUIT) {
            m_fruit.new_position();
            this->lengthen_snake_left();
            return;
        }

        this->lengthen_snake_left();
        this->pop_snake_body();
    }
    void move_right() noexcept
    {
        vec2 head_pos = m_positions.front();
        vec2 new_pos{head_pos.i, head_pos.j + 1};

        if(m_field(new_pos.i, new_pos.j) == field_t::cell_type::BODY) {
            m_died = true;
            return;
        }
        if(new_pos.j >= g_field_width) {
            m_died = true;
            return;
        }

        if(m_field(new_pos.i, new_pos.j) == field_t::cell_type::FRUIT) {
            m_fruit.new_position();
            this->lengthen_snake_right();
            return;
        }

        this->lengthen_snake_right();
        this->pop_snake_body();
    }

    inline bool died() const noexcept
    {
        return m_died;
    }

    inline int length() const noexcept
    {
        return static_cast<int>(m_positions.size());
    }

    void update() noexcept
    {
        this->update_snake_on_field();
    }
};

class game_t : public sdlge::application_t
{
  private:
    field_t m_field;
    fruit_t m_fruit;
    snake_t m_snake;

    enum snake_direction_type : int
    {
        UP = 0,
        DOWN, 
        LEFT,
        RIGHT
    }
    m_snake_direction, m_new_direction;

    int m_score;

    float m_waited_time;

    void draw_field() noexcept
    {
        for(int i = 0; i < g_field_height; ++i) {
            for(int j = 0; j < g_field_width; ++j) {
                auto color = sdlge::BLACK;
                switch(m_field(i, j)) {
                case field_t::cell_type::HEAD:
                    color = sdlge::GREEN;
                    break;
                case field_t::cell_type::BODY:
                    color = sdlge::LIME;
                    break;
                case field_t::cell_type::FRUIT:
                    color = sdlge::RED;
                    break;
                default:
                    break;
                }

                rectangle(j * g_tile_width, i * g_tile_height, g_tile_width,
                          g_tile_height, color);
            }
        }
    }

    void handle_input() noexcept
    {
        if(key_pressed(SDL_SCANCODE_UP)) {
            if(m_snake_direction != DOWN) {
                m_new_direction = UP;
            }
        }
        else if(key_pressed(SDL_SCANCODE_DOWN)) {
            if(m_snake_direction != UP) {
                m_new_direction = DOWN;
            }
        }
        else if(key_pressed(SDL_SCANCODE_LEFT)) {
            if(m_snake_direction != RIGHT) {
                m_new_direction = LEFT;
            }
        }
        else if(key_pressed(SDL_SCANCODE_RIGHT)) {
            if(m_snake_direction != LEFT) {
                m_new_direction = RIGHT;
            }
        }
    }

    void move_snake() noexcept
    {
        switch(m_snake_direction) {
        case UP:
            m_snake.move_up();
            break;
        case DOWN:
            m_snake.move_down();
            break;
        case LEFT:
            m_snake.move_left();
            break;
        case RIGHT:
            m_snake.move_right();
            break;
        default:
            break;
        }
    }

  protected:
    virtual bool update(float const t_elapsed_time) noexcept
    {
        if(key_pressed(SDL_SCANCODE_ESCAPE)) {
            return false;
        }

        set_title(
            (std::string{"Snake! Score: "} + std::to_string(m_snake.length()))
                .c_str());

        m_field.clear();

        m_snake.update();
        m_fruit.update();

        this->draw_field();
        this->handle_input();

        m_waited_time += t_elapsed_time;

        if(m_waited_time >= g_max_wait_time_ms / 1000.f) {
            m_snake_direction = m_new_direction;
            this->move_snake();
            m_waited_time = 0.f;
        }

        return !m_snake.died();
    }

  public:
    game_t()
        : application_t{g_width, g_height, "Snake"}
        , m_field{}
        , m_fruit{m_field}
        , m_snake{m_field, m_fruit}
        , m_snake_direction{UP}
        , m_new_direction{UP}
        , m_score{0}
        , m_waited_time{0.f}
    {
        m_snake.update();
        // Fruit is generated first, then snake, this
        // is called again to avoid having the snake over the fruit
        m_fruit.new_position();
    }
    ~game_t()
    {
        std::cout << "Score: " << m_snake.length() << '\n';
    }
};

class start_menu_t : public sdlge::application_t
{
  private:
    sdlge::font_t m_font;
    sdlge::text_t m_text;

    static int constexpr m_text_size{32};

  protected:
    virtual bool update(float const) noexcept
    {
        if(key_pressed(SDL_SCANCODE_ESCAPE)) {
            return false;
        }

        render_text(m_text, sdlge::vec2{g_width / 2 - m_text.width() / 2,
                                        g_height / 2 - m_text.height() / 2});

        outline_rectangle(g_width / 2 - m_text.width() / 2 - 50,
                          g_height / 2 - m_text.height() / 2 - 50,
                          m_text.width() + 2 * 50, m_text.width() + 2 * 50);

        if(key_pressed(SDL_SCANCODE_RETURN)) {
            return false;
        }

        return true;
    }

  public:
    start_menu_t(sdlge::application_t& t_app)
        : application_t{t_app}
        , m_font{load_font(
              "../res/fonts/elaine-sans/hinted-ElaineSans-Light.ttf",
              m_text_size)}
        , m_text{m_font, "Play", sdlge::WHITE}
    {
    }
};

int main()
{
    g_scenes.push_back(std::make_unique<game_t>());
    g_scenes.push_back(std::make_unique<start_menu_t>(*g_scenes.front()));

    while(!g_scenes.empty()) {
        g_scenes.back()->loop();
        g_scenes.pop_back();
    }
}
