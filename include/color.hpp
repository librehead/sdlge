#pragma once
#ifndef SDLGE_COLOR_HPP
#define SDLGE_COLOR_HPP

namespace sdlge {
    class color_t
    {
      private:
        int m_r{255};
        int m_g{255};
        int m_b{255};
        int m_a{255};

      public:
        color_t() noexcept = default;
        color_t(int const t_r, int const t_g, int const t_b,
                int const t_a = 255) noexcept;
        color_t(color_t const&) noexcept = default;
        color_t(color_t&&) noexcept = default;
        ~color_t() noexcept = default;

        color_t& operator=(color_t const&) = default;
        color_t& operator=(color_t&&) = default;

        inline int red() const noexcept
        {
            return m_r;
        }
        inline int green() const noexcept
        {
            return m_g;
        }
        inline int blue() const noexcept
        {
            return m_b;
        }
        inline int alpha() const noexcept
        {
            return m_a;
        }

        bool operator==(color_t const& t_color) const noexcept;
        bool operator!=(color_t const& t_color) const noexcept;
    };
} // namespace sdlge

#endif
