#pragma once
#ifndef SDLGE_EVENT_HPP
#define SDLGE_EVENT_HPP

#include <map>

#include "SDL2/SDL.h"

namespace sdlge {
    class event_t
    {
      private:
        std::map<SDL_Scancode, bool> m_keys_pressed;
        std::map<SDL_Scancode, bool> m_keys_held;
        std::map<SDL_Scancode, bool> m_keys_released;

        bool m_quit{false};

        int m_mouse_x{-1};
        int m_mouse_y{-1};
        int m_mouse_state{0};

      public:
        event_t() = default;
        event_t(event_t const&) = default;
        ~event_t() noexcept = default;

        void update();
        void reset() noexcept;

        inline bool quit() const noexcept
        {
            return m_quit;
        }

        bool was_key_pressed(SDL_Scancode const t_key) const noexcept;
        bool is_key_held(SDL_Scancode const t_key) const noexcept;
        bool was_key_released(SDL_Scancode const t_key) const noexcept;

        inline int get_mouse_x() const noexcept
        {
            return m_mouse_x;
        }

        inline int get_mouse_y() const noexcept
        {
            return m_mouse_y;
        }

        inline bool left_mouse_button_pressed() const noexcept
        {
            return m_mouse_state & SDL_BUTTON(SDL_BUTTON_LEFT);
        }

        inline bool middle_mouse_button_pressed() const noexcept
        {
            return m_mouse_state & SDL_BUTTON(SDL_BUTTON_MIDDLE);
        }

        inline bool right_mouse_button_pressed() const noexcept
        {
            return m_mouse_state & SDL_BUTTON(SDL_BUTTON_RIGHT);
        }
    };
} // namespace sdlge

#endif
