#pragma once
#ifndef SDLGE_RENDERER_HPP
#define SDLGE_RENDERER_HPP

#include "color.hpp"

struct SDL_Renderer;

namespace sdlge {
    class window_t;

    class renderer_t
    {
      private:
        SDL_Renderer* m_renderer{nullptr};
        window_t& m_window;
        color_t m_clear_color{0, 0, 0, 255};

        bool m_borrowed_resource{false};

        void set_render_color(color_t const& t_color) noexcept;

      public:
        renderer_t() = delete;
        renderer_t(sdlge::window_t& t_window) noexcept;
        renderer_t(renderer_t& t_other) noexcept;
        ~renderer_t() noexcept;

        void render_point(int const t_x, int const t_y,
                          color_t const& t_color = color_t{}) noexcept;
        void render_line(int const t_x0, int const t_y0, int const t_x1,
                         int const t_y1,
                         color_t const& t_color = color_t{}) noexcept;
        void render_rectangle(int const t_x, int const t_y, int const t_w,
                              int const t_h,
                              color_t const& t_color = color_t{}) noexcept;
        void
        render_outline_rectangle(int const t_x, int const t_y, int const t_w,
                                 int const t_h,
                                 color_t const& t_color = color_t{}) noexcept;
        void render_triangle(int const t_x0, int const t_y0, int const t_x1,
                             int const t_y1, int const t_x2, int const t_y2,
                             color_t const& t_color = color_t{}) noexcept;
        void
        render_outline_triangle(int const t_x0, int const t_y0, int const t_x1,
                                int const t_y1, int const t_x2, int const t_y2,
                                color_t const& t_color = color_t{}) noexcept;
        void render_circle(int const t_x, int const t_y, int const t_radius,
                           color_t const& t_color = color_t{}) noexcept;
        void render_outline_circle(int const t_x, int const t_y,
                                   int const t_radius,
                                   color_t const& t_color = color_t{}) noexcept;

        void set_clear_color(color_t const& t_color) noexcept;
        void clear() noexcept;
        void update() noexcept;

        SDL_Renderer* get() noexcept;
        SDL_Renderer* get() const noexcept;

        inline window_t& window() noexcept
        {
            return m_window;
        }
        inline window_t const& window() const noexcept
        {
            return m_window;
        }
    };
} // namespace sdlge

#endif
