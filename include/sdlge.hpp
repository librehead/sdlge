#pragma once
#ifndef SDLGE_HPP
#define SDLGE_HPP

#include "event.hpp"
#include "font.hpp"
#include "renderer.hpp"
#include "text.hpp"
#include "vec2.hpp"
#include "window.hpp"

namespace sdlge {
    class application_t
    {
      private:
        window_t m_window{640, 480, "SDLGE app"};
        renderer_t m_renderer{m_window};
        event_t m_event{};

        float m_fps{0.0f};
        float m_avg_fps{0.0f};

      protected:
        application_t() = default;
        application_t(int const t_width, int const t_height,
                      char const* const t_tile) noexcept;
        application_t(application_t& t_other) noexcept;

        inline void point(int const t_x, int const t_y,
                          color_t const& t_color = color_t{}) noexcept
        {
            m_renderer.render_point(t_x, t_y, t_color);
        }

        inline void line(int const t_x0, int const t_y0, int const t_x1,
                         int const t_y1,
                         color_t const& t_color = color_t{}) noexcept
        {
            m_renderer.render_line(t_x0, t_y0, t_x1, t_y1, t_color);
        }

        inline void rectangle(int const t_x, int const t_y, int const t_w,
                              int const t_h,
                              color_t const& t_color = color_t{}) noexcept
        {
            m_renderer.render_rectangle(t_x, t_y, t_w, t_h, t_color);
        }

        inline void
        outline_rectangle(int const t_x, int const t_y, int const t_w,
                          int const t_h,
                          color_t const& t_color = color_t{}) noexcept
        {
            m_renderer.render_outline_rectangle(t_x, t_y, t_w, t_h, t_color);
        }

        inline void triangle(int const t_x0, int const t_y0, int const t_x1,
                             int const t_y1, int const t_x2, int const t_y2,
                             color_t const& t_color = color_t{}) noexcept
        {
            m_renderer.render_triangle(t_x0, t_y0, t_x1, t_y1, t_x2, t_y2,
                                       t_color);
        }

        inline void
        outline_triangle(int const t_x0, int const t_y0, int const t_x1,
                         int const t_y1, int const t_x2, int const t_y2,
                         color_t const& t_color = color_t{}) noexcept
        {
            m_renderer.render_outline_triangle(t_x0, t_y0, t_x1, t_y1, t_x2,
                                               t_y2, t_color);
        }

        inline void circle(int const t_x, int const t_y, int const t_radius,
                           color_t const& t_color = color_t{}) noexcept
        {
            m_renderer.render_circle(t_x, t_y, t_radius, t_color);
        }

        inline void outline_circle(int const t_x, int const t_y,
                                   int const t_radius,
                                   color_t const& t_color = color_t{}) noexcept
        {
            m_renderer.render_outline_circle(t_x, t_y, t_radius, t_color);
        }

        virtual bool update(float const t_elapsed_time) noexcept = 0;

        inline void set_title(char const* const t_title) noexcept
        {
            m_window.set_title(t_title);
        }

        inline float query_fps() const noexcept
        {
            return m_fps;
        }

        inline float query_avg_fps() const noexcept
        {
            return m_avg_fps;
        }

        inline bool key_pressed(SDL_Scancode const t_key) const noexcept
        {
            return m_event.was_key_pressed(t_key);
        }

        inline bool key_held(SDL_Scancode const t_key) const noexcept
        {
            return m_event.is_key_held(t_key);
        }

        inline bool key_released(SDL_Scancode const t_key) const noexcept
        {
            return m_event.was_key_released(t_key);
        }

        inline bool mouse_left() const noexcept
        {
            return m_event.left_mouse_button_pressed();
        }

        inline bool mouse_middle() const noexcept
        {
            return m_event.middle_mouse_button_pressed();
        }

        inline bool mouse_right() const noexcept
        {
            return m_event.right_mouse_button_pressed();
        }

        inline int mouse_x() const noexcept
        {
            return m_event.get_mouse_x();
        }

        inline int mouse_y() const noexcept
        {
            return m_event.get_mouse_y();
        }

        inline font_t load_font(char const* const t_path,
                                int const t_size) noexcept
        {
            return font_t{t_path, t_size, m_renderer};
        }

        void render_text(text_t& t_text, vec2 const& t_pos) noexcept;

      public:
        virtual ~application_t() noexcept = default;

        void loop();

        inline window_t const& window() const noexcept
        {
            return m_window;
        }
        inline renderer_t& renderer() noexcept
        {
            return m_renderer;
        }
    };

    const sdlge::color_t RED = sdlge::color_t{255, 0, 0};
    const sdlge::color_t GREEN = sdlge::color_t{0, 255, 0};
    const sdlge::color_t BLUE = sdlge::color_t{0, 0, 255};
    const sdlge::color_t WHITE = sdlge::color_t{255, 255, 255};
    const sdlge::color_t BLACK = sdlge::color_t{0, 0, 0};
    const sdlge::color_t CYAN = sdlge::color_t{0, 188, 212};
    const sdlge::color_t YELLOW = sdlge::color_t{255, 235, 59};
    const sdlge::color_t LIME = sdlge::color_t{205, 220, 57};
    const sdlge::color_t TEAL = sdlge::color_t{0, 150, 136};
    const sdlge::color_t PURPLE = sdlge::color_t{156, 39, 176};
    const sdlge::color_t PINK = sdlge::color_t{236, 64, 122};
    const sdlge::color_t INDIGO = sdlge::color_t{63, 81, 181};
    const sdlge::color_t AMBER = sdlge::color_t{255, 193, 7};
    const sdlge::color_t ORANGE = sdlge::color_t{255, 152, 0};
    const sdlge::color_t BROWN = sdlge::color_t{121, 85, 72};
    const sdlge::color_t GREY = sdlge::color_t{158, 158, 158};
} // namespace sdlge

#endif
