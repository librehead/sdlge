#pragma once
#ifndef SDLGE_WINDOW_HPP
#define SDLGE_WINDOW_HPP

struct SDL_Window;
struct SDL_Surface;

namespace sdlge {
    class window_t
    {
      private:
        int m_width{0};
        int m_height{0};

        SDL_Window* m_window{nullptr};

        char const* m_title{""};

        bool m_borrowed_resource{false};

        void initialize_sdl() noexcept;
        void update_window_title() noexcept;

      public:
        window_t() noexcept;
        window_t(int const t_width, int const t_height,
                 char const* const t_title) noexcept;
        window_t(window_t const& t_other);
        ~window_t() noexcept;

        SDL_Window* get() noexcept;
        SDL_Window* get() const noexcept;

        SDL_Surface* get_surface() noexcept;
        SDL_Surface* get_surface() const noexcept;

        // void update() noexcept;
        void set_title(char const* const t_title) noexcept;

        inline int width() const noexcept
        {
            return m_width;
        }

        inline int height() const noexcept
        {
            return m_width;
        }

        inline char const* title() const noexcept
        {
            return m_title;
        }
    };
} // namespace sdlge

#endif
