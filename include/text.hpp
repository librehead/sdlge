#pragma once
#ifndef SDLGE_TEXT_HPP
#define SDLGE_TEXT_HPP

#include "color.hpp"
#include "vec2.hpp"

#include "SDL2/SDL.h"

namespace sdlge {
    class font_t;

    class text_t
    {
      private:
        font_t& m_font;
        color_t m_color{};
        SDL_Texture* m_texture{nullptr};
        // x is width, y is height
        vec2 m_dimension{};

      public:
        text_t() = delete;
        text_t(font_t& t_font, char const* const t_str = "",
               color_t const& t_color = color_t{}) noexcept;
        ~text_t() noexcept;

        inline int width() const noexcept
        {
            return m_dimension.x;
        }
        inline int height() const noexcept
        {
            return m_dimension.y;
        }

        inline color_t const& color() const noexcept
        {
            return m_color;
        }
        inline color_t& color() noexcept
        {
            return m_color;
        }

        inline SDL_Texture* get_texture() noexcept
        {
            return m_texture;
        }
        inline SDL_Texture* get_texture() const noexcept
        {
            return m_texture;
        }
    };
} // namespace sdlge

#endif
