#pragma once
#ifndef SDLGE_VEC2_HPP
#define SDLGE_VEC2_HPP

namespace sdlge {
    struct vec2
    {
        int x{0};
        int y{0};

        vec2() noexcept = default;
        vec2(int const t_x, int const t_y) noexcept;
        vec2(vec2 const&) noexcept = default;
        vec2(vec2&&) noexcept = default;
        ~vec2() noexcept = default;

        vec2& operator=(vec2 const&) = default;
        vec2& operator=(vec2&&) = default;
    };
} // namespace sdlge

#endif
