#pragma once
#ifndef SDLGE_FONT_HPP
#define SDLGE_FONT_HPP

#include "SDL2/SDL_ttf.h"

namespace sdlge {
    class renderer_t;

    class font_t
    {
      private:
        TTF_Font* m_font{nullptr};
        renderer_t& m_renderer;

      public:
        font_t() = delete;
        font_t(char const* const t_path, int const t_size,
               renderer_t& m_renderer) noexcept;
        font_t(font_t const&) = default;
        font_t(font_t&&) = default;
        ~font_t() noexcept;

        inline TTF_Font* get() noexcept
        {
            return m_font;
        }
        inline TTF_Font* get() const noexcept
        {
            return m_font;
        }

        inline renderer_t& get_renderer() noexcept
        {
            return m_renderer;
        }
        inline renderer_t& get_renderer() const noexcept
        {
            return m_renderer;
        }
    };
} // namespace sdlge

#endif
